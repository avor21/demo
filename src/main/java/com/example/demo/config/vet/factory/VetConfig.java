package com.example.demo.config.vet.factory;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.Map;


@Configuration
@PropertySource(value = "classpath:vetis-dict-mapping.yml", factory = YamlPropertySourceFactory.class)
@ConfigurationProperties(prefix = "vetis")
public class VetConfig {
    private Map<String, Dict> dicts;

    public Map<String, Dict> getDicts() {
        return this.dicts;
    }

    public void setDicts(Map<String, Dict> dicts) {
        this.dicts = dicts;
    }

    public static class Dict {
        private String name;
        private String path;
        private Map<String, String> fieldMapping;

        public String getName() {
            return this.name;
        }

        public String getPath() {
            return this.path;
        }

        public Map<String, String> getFieldMapping() {
            return this.fieldMapping;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public void setFieldMapping(Map<String, String> fieldMapping) {
            this.fieldMapping = fieldMapping;
        }
    }
}
