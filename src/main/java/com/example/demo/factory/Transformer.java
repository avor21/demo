package com.example.demo.factory;

public interface Transformer {

    void doTransform();

}