package com.example.demo.factory;

import com.example.demo.factory.vetis.TransformerVet;
import org.springframework.context.annotation.Scope;

import java.util.Objects;

@Scope(value = "singleton")
public class TransformFactory {
    private static TransformFactory instance;
    public static TransformFactory getInstance() {
        if (Objects.isNull(instance)) {
            instance = new TransformFactory();
        }
        return instance;
    }

    private TransformFactory() {

    }

    public Transformer getTransformer() {
        return new TransformerVet();
    }
}
