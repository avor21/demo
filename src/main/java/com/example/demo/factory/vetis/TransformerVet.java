package com.example.demo.factory.vetis;

import com.example.demo.config.vet.factory.VetConfig;
import com.example.demo.factory.Transformer;
import org.springframework.beans.factory.annotation.Autowired;


public class TransformerVet implements Transformer {

    @Autowired
    private VetConfig vetConfig;

    public void doTransform() {
        System.out.println(vetConfig);
    }

}
